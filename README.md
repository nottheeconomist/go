# Go Utils

A personal toolkit to make Go a little easier to use.

## Installation

Either use the `go get` tool to install each package

    $ go get gitlab.com/nottheeconomist/go/utils

or simply make the directory in your GOPATH and clone this repo into it.

    $ mkdir -p $GOPATH/src/gitlab.com/nottheeconomist
    $ cd $GOPATH/src/gitlab.com/nottheeconomist
    $ git clone git@gitlab.com:nottheeconomist/go
    // or if you prefer https
    $ git clone https://<username>@gitlab.com/nottheeconomist/go
