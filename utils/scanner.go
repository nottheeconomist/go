// Helper API for bufio.Scanner set up.
package utils

import (
	"bufio"
	"io"
	"strconv"
)

func NewScanner(rd io.Reader, fn bufio.SplitFunc) chan string {
	ch := make(chan string)
	scanner := bufio.NewScanner(rd)
	scanner.Split(fn)
	go func() {
		defer close(ch)
		for scanner.Scan() {
			ch <- scanner.Text()
		}
	}()
	return ch
}

func NewIntScanner(rd io.Reader, fn bufio.SplitFunc) chan int {
	ch := make(chan int)
	scanner := bufio.NewScanner(rd)
	scanner.Split(fn)
	go func() {
		defer close(ch)
		for scanner.Scan() {
			t := scanner.Text()
			if i, err := strconv.Atoi(t); err != nil {
				panic(err)
			} else {
				ch <- i
			}
		}
	}()
	return ch
}

type ParseFunc func(string) interface{}

func NewCustomScanner(rd io.Reader, fn bufio.SplitFunc, parsefn ParseFunc) chan interface{} {
	ch := make(chan interface{})
	scanner := bufio.NewScanner(rd)
	scanner.Split(fn)
	go func() {
		defer close(ch)
		for scanner.Scan() {
			data := parsefn(scanner.Text())
			ch <- data
		}
	}()
	return ch
}
